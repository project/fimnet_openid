<?php

namespace Drupal\fimnet_openid\Plugin\OpenIDConnectClient;

use Drupal\Core\Form\FormStateInterface;
use Drupal\openid_connect\Plugin\OpenIDConnectClientBase;
use Drupal\fimnet_openid\FireApiClient\FireApiClient;

/**
 * Fimnet OpenID Connect client.
 *
 * Used primarily to login to Drupal sites powered by oauth2_server or PHP
 * sites powered by oauth2-server-php.
 *
 * @OpenIDConnectClient(
 *   id = "fimnet",
 *   label = @Translation("Fimnet")
 * )
 */
class OpenIDConnectFimnetClient extends OpenIDConnectClientBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'auth_base_url' => '',
      'base_url' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['base_url'] = [
      '#title' => $this->t('API Base url'),
      '#type' => 'textfield',
      '#default_value' => $this->configuration['base_url'],
    ];
    $form['auth_base_url'] = [
      '#title' => $this->t('Auth Base url'),
      '#type' => 'textfield',
      '#default_value' => $this->configuration['auth_base_url'],
    ];

    return $form;
  }

  /**
   * Overrides OpenIDConnectClientBase::getEndpoints().
   */
  public function getEndpoints() {
    $auth_base_url = $this->configuration['auth_base_url'];

    $api = new FireApiClient([
      'auth_base_url' => $auth_base_url,
    ]);
    return [
      'authorization' => $auth_base_url . $api::AUTHORIZE_ENDPOINT,
      'token' => $auth_base_url . $api::TOKEN_ENDPOINT,
    ];
  }

  /**
   * Implements OpenIDConnectClientInterface::decodeIdToken().
   */
  public function decodeIdToken($id_token) {
    list($headerb64, $claims64, $signatureb64) = explode('.', $id_token);
    $claims64 = str_replace(['-', '_'], ['+', '/'], $claims64);
    $claims64 = base64_decode($claims64);
    // Store tokens for FireApi.
    $this->tokens = json_decode($claims64, TRUE);
    return $this->tokens;
  }

  /**
   * Implements OpenIDConnectClientInterface::retrieveUserInfo().
   */
  public function retrieveUserInfo($access_token) {
    $endpoints = $this->getEndpoints();

    $api = new FireApiClient([
      'base_url' => $this->configuration['base_url'],
      'auth_base_url' => $this->configuration['auth_base_url'],
      'client_id' => $this->configuration['client_id'],
      'client_secret' => $this->configuration['client_secret'],
      'access_token' => $access_token,
    ]);

    if (empty($this->tokens['sub'])) {
      return;
    }
    try {
      $response = $api->get('/users/' . $this->tokens['sub']);

      $email = sprintf('%s@%s', $response->fimnet_id, $this->configuration['client_id']);

      if (!empty($response->fimnet_email) && valid_email_address($response->fimnet_email)) {
        $email = $response->fimnet_email;
      }
      return [
        'email' => $email,
      ];
    }
    catch (Exception $e) {
      return FALSE;
    }
  }

}
